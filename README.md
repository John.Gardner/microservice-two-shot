# Wardrobify

Team:

* John Gardner - Hats
* Sophie Nguyen - Shoes

## Design

1. Git clone into your local repository
2. cd into it
3. run docker compose build
4. Create a volume and name it pgdata
5. Run the images
6. Open browser to localhost:3000 to make sure it’s running
7. In insomnia, create a couple bins and locations to use
	* To create a bin
		* Create a post request pointing to http://localhost:8100/api/bins/
		* Change the body to json and  pass  in data like this
			 * {"closet_name": "Orange", "bin_number": 3, "bin_size": 10}
	* To create a closet
		* Create a post request pointing to http://localhost:8100/api/locations/
		* Change the body to json and  pass  in data like this
			 * {"closet_name": "Orange", "section_number": 3, "shelf_number": 10}
8. After a creating a couple locations and bins to use, you can now create shoes and hats

## Hats microservice

1. In settings.py of the Hats Django project:
    Install the hats_rest application into INSTALLED_APPS
    Add "wardrobe-api" and "localhost" to ALLOWED_HOSTS
2. In hats_rest/models, create a Hat model with the following properties and field types:
    fabric: Charfield,
    style: Charfield
    color: Charfield,
    url: URLField
    location: Foreignkey to LocationVO from the wardrobe microservice with the related name "location" and protects on delete
3. In hats_rest/models, create a LocationVO model with the following properties and field types:
    import_href: CharField,
    closet_name: CharField,
    section_number: PositiveSmallIntegerField,
    shelf_number: PositiveSmallIntegerField
4. Register the models in hats_rest/admin.py
5. Start the servers using the following docker commands
    docker volume create pgdata
    docker-compose up --build
 6. When the servers are running, make migrations for the added models in the CLI of docker for the hats api container
 7. Set up polling so the wardrobe microservice sends a list of Locations every minute. The Hat model can access LocationVO through a foreign key
 8. Create API Views so the user can do the following:
    Get a list of shoes
    Create a shoe
    Delete a shoe
9. Create REACT components to display a new Hat Form and list hats
10. Create a button on the HatsList component so the user is able to delete a hat
11. Update the existing nav links to link to the new components
12. Spend way too long customizing the CSS and realizing you hate Bootstrap and wanted to create your own CSS modules

## Shoes microservice

1. In settings.py of the Shoes Django project:
    Install the shoes_rest application into INSTALLED_APPS
    Add "wardrobe-api" and "localhost" to ALLOWED_HOSTS
2. In shoes_rest/models, create a Shoe model with the following properties and field types:
    manufacturer: Charfield,
    model: Charfield
    color: Charfield,
    picture_url: URLField
    bin: Foreignkey to BinVO from the wardrobe microservice with the related name "shoes" and cascades on delete
3. In shoes_rest/models, create a BinVO model with the following properties and field types:
    import_href: CharField,
    closet_name: CharField,
    bin_number: IntegerField,
    bin_size: PositiveSmallIntegerField
4. Register the models in shoe_rest/admin.py
5. Start the servers using the following docker commands
    docker compose build
    docker volume create pgdata
    docker compose up
 6. When the servers are running, make migrations for the added models in the CLI of docker for the   shoes api container
 7. Set up polling so the wardrobe microservice sends a list of Bins every minute or so. The Shoe model can access BinVO through a foreign key
 8. Create API Views so the user can do the following:
    Get a list of shoes
    Create a shoe
    Delete a shoe
9. Create REACT components to display a new Shoe Form and list shoes
10. Create a button on the ShoeList component so the user is able to delete a shoe
11. Update the existing nav links to link to the new components
