from django.apps import AppConfig


class ShoeApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shoes_rest'
