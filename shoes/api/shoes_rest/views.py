from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo

from common.json import ModelEncoder
from .models import Shoe, BinVO
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model", "manufacturer", "color", "id", "picture_url"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model",
        "manufacturer",
        "color",
        "bin",
        ]
    encoders = {
        "bin": BinVODetailEncoder(),
        }

@require_http_methods(["GET","POST"])
def api_list_shoe(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder = ShoeListEncoder
        )
    else:
        content=json.loads(request.body)
        print("CONTENT:::::",content)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin id"},
            status=400,
            )
        picture_url = get_photo(content["color"],content["model"],content["manufacturer"])
        content.update(picture_url)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_shoe(request,pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
