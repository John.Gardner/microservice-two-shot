from django.urls import path
from .views import api_list_shoe, api_delete_shoe

urlpatterns = [
    path("shoes/",api_list_shoe,name='api_list_shoes'),
    path("shoes/<int:pk>/",api_delete_shoe,name='api_delete_shoe'),
]
