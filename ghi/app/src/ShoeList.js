function ShoeList(props) {

  async function deleteShoe (id) {
    console.log("CLICKED DELETE BUTTON")
    const deleteUrl = `http://localhost:8080/api/shoes/${id}/`
    const fetchConfig = {
      method: "delete"
    }

    const response = await fetch(deleteUrl, fetchConfig)
    console.log("RESPONSE::", response)

    if(response.ok) {
      const response = await fetch('http://localhost:8080/api/shoes/');
      console.log(props);
      props.loadShoes();

    }

  }



    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture Url</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
        {props.shoes.map(shoe => {
          return (
            <tr key={shoe.model}>
              <td>{ shoe.model }</td>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.color }</td>
              <td><a href={ shoe.picture_url }>Click for picture</a></td>
              <td><button onClick={() => deleteShoe(shoe.id)} >Delete</button></td>
            </tr>
          );
        })}
        </tbody>
        </table>

    )

}

export default ShoeList;
