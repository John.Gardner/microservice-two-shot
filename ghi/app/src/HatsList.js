import React from 'react';
import { Link } from 'react-router-dom';
import styles from './mystyle.module.css';


const deleteHat = async (id) => {
    fetch(`http://localhost:8090/hats/${id}/`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

function HatsColumns(props) {
    // HatsColumns
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.id} className={styles.card}>
                        <div>
                            <h3 className={styles.cardTitle}>{hat.color} {hat.style} hat</h3>
                        </div>
                        <div>
                            <img src={hat.url} className={styles.cardImage}></img>
                            <h6 className={styles.cardLocation}>{hat.location.closet_name} Closet (sec: {hat.location.section_number} shelf: {hat.location.shelf_number})</h6>
                        </div>
                        <div className={styles.cardButton}>
                            <button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">Delete</button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

class HatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatsColumns: [[], [], [], []]
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/hats/${hat.id}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatsColumns = [[], [], [], []]

                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatsColumns[i].push(details)
                        i += 1;
                        if (i > 3) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }

                this.setState({ hatsColumns: hatsColumns })
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="text-center bg-info pb-4">
                    <h1 className="display-5 fw-bold">Getcha Hats Here</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                            This is all your hats!
                        </p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a New Hat</Link>
                        </div>
                    </div>
                </div>
                <div className="container text-center pt-4 pb-4">
                    <h2>Woah, That's a lot of Hats!</h2>
                </div>
                <div className='container'>
                    <div className="row">
                        {this.state.hatsColumns.map((list, index) => {
                            return (
                                <HatsColumns key={index} list={list} />
                            );
                        })}
                    </div>
                </div>
            </>
        )
    }
}

export default HatsList