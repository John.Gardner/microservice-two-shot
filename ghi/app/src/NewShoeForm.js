import React, { Component } from 'react'

class NewShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            model: '',
            manufacturer: '',
            color: '',
            bins: []
        };

        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        }

async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state};
    delete data.bins;
    console.log(data)

    const shoeurl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": 'application/json'
        }
    };

    const response = await fetch (shoeurl,fetchConfig);
    if (response.ok) {
        const newShoe = await response.json();
        console.log("NEW SHOE::", newShoe);

        const cleared = {
            model: '',
            manufacturer: '',
            color: '',
            bin: '',
        };
        this.setState(cleared)
    }
}

handleModelChange(event) {
    const value = event.target.value;
    this.setState({model: value})
}

handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer: value})
}

handleModelChange(event) {
    const value = event.target.value;
    this.setState({model: value})
}

handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
}

handleBinChange(event) {
    const value = event.target.value;
    this.setState({bin: value})
}


async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url)
        console.log("response::", response)

        if (response.ok) {
            const data = await response.json();
            console.log("DATA::", data)
            this.setState({bins:data.bins})
        }
    }



    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={this.handleSubmit}id="create-shoe-form">
                    <div className="form-floating mb-3">
                    <input placeholder="Model" required type="text" name="model" id="model" className="form-control" onChange={this.handleModelChange} value={this.state.model} />
                    <label htmlFor="model">Model</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" onChange={this.handleManufacturerChange} value = {this.state.manufacturer}/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input placeholder="Color" required type="text" name="color" id="color" className="form-control" onChange={this.handleColorChange} value={this.state.color}/>
                    <label htmlFor="color">Color</label>
                    </div>
                    <div className="mb-3">
                    <select required name="bin" id="bin" className="form-select" onChange={this.handleBinChange} value={this.state.bin}>
                        <option value="">Choose a bin</option>
                        {this.state.bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                {bin.closet_name} closet - Bin {bin.id}
                                </option>
                            )
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
            )
        }
    }


export default NewShoeForm
