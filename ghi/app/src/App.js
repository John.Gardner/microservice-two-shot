import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewShoeForm from './NewShoeForm';
import ShoeList from './ShoeList';
import HatsList from './HatsList';
import HatForm from './HatForm';

function App(props) {
  if (props.shoes===undefined){
  return null
}
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">

        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element = {<ShoeList  loadShoes={props.loadShoes} shoes={props.shoes} />} />
            <Route path="new" element = {<NewShoeForm />}/>
          </Route>
          <Route path="/hats/" element={<HatsList />} />
          <Route path="/hats/new/" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
