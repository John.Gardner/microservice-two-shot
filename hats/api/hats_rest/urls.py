from django.contrib import admin
from django.urls import path
from .views import listHats, hatDetail

urlpatterns = [
    path('', listHats, name="hats"),
    path("<int:pk>/", hatDetail, name="hatDetail")
]
