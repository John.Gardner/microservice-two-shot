from django.contrib import admin
from .models import Test, Hat, LocationVO

# Register your models here.


@admin.register(Test, Hat, LocationVO)
class TestAdmin(admin.ModelAdmin):
    pass


class HatAdmin(admin.ModelAdmin):
    pass


class LocationVOAdmin(admin.ModelAdmin):
    pass
