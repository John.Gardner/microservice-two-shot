from common.keys import PEXELS_API_KEY
import requests
import json


def get_photo(style, color):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f" {color} {style} hat",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"url": None}
